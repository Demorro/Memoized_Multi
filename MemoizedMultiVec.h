#pragma once
#include <vector>
#include <functional>

/**
 * This container is designed to be a nice interface to create a set of vectors that track a "prime" vector container according to a set of transformations.
 * This might be useful if you, for example, have a set of matrix transforms, and you want to store vertices in a container, but also keep a memoized version
 * of all the transformed vertices available for easy access relative to the prime container. Transformations are done at insert time, so access is quick after.
 * There is no need for the memoized vectors to be of similar types, and indeed this might be a handy way to store the same data as various types without having to do
 * on-the-fly conversion. Types of the memoized vectors will be of type std::result_of<Transformation(DataType)>
 *
 * Example usage : 
 * auto times10 = [](double i) {return i * 10; };
 * auto toString = [](double i) {return std::to_string(i); };
 * MemoizedMultiVec<double, decltype(times10), decltype(toString)> memoizedVec(std::move(times10), std::move(toString));
 * 
 * This creates 3 vectors, the prime vector of type double, then another vector of type double, and a vector of type string.
 * Any insertion to the vector will now also insert a double in the second vector, which is 10 times bigger than the first, and a string representation of the data in the third vector
 *
 * memoizedVec.push_back(5);
 * memoizedVec.getPrimeData()[0] == 5.0;
 * memoizedVec.getMemoizedData<0>()[0] == 50.0;
 * memoizedVec.getMemoizedData<1>[0] == "5.0"
 *
 * Transformations can be lambdas, std::functions, or really any callable object. Lambdas are probably best.
 * 
 * Notes : A BIG limitation of this class is that you can't really use any modifying standard algorithms with it. I tried to do something with custom iterators, but it defeated me
 * Currently the only things you can use are the wrapped functions, if you want to say, stable partition this, you're shit out of luck.
 * Still quite a useful utility tho, in my opinion.
 */

template<typename DataType, typename... Transformation>
class MemoizedMultiVec
{
public:
    MemoizedMultiVec(Transformation&&... transformations) 
        : m_memoizedData(std::make_pair(std::forward<Transformation>(transformations), std::vector<typename std::result_of<Transformation(DataType)>::type>())...) {}

	const std::vector<DataType>& getPrimeData()
	{
		return m_primeData;
	}

	template<int Memoization>
	decltype(auto) getMemoizedData()
	{
		return std::get<Memoization>(m_memoizedData).second;
	}

	typename std::vector<DataType>::const_iterator begin() { return m_primeData.begin();}
	typename std::vector<DataType>::const_iterator end() { return m_primeData.end(); }

	void clear()
	{
		m_primeData.clear();
		clear_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>();
	}

	const typename std::vector<DataType>::const_iterator insert(typename std::vector<DataType>::const_iterator _Where, DataType&& _Val)
	{
		const unsigned int insertDistance = std::distance(m_primeData.cbegin(), _Where);
		const auto insertReturnIt = m_primeData.insert(_Where, std::forward<DataType>(_Val));
		insert_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(insertDistance, std::forward<DataType>(_Val));
		return insertReturnIt;
	}

	const typename std::vector<DataType>::const_iterator emplace(typename std::vector<DataType>::const_iterator _Where, DataType&& _Val)
	{
		const unsigned int emplaceDistance = std::distance(m_primeData.cbegin(), _Where);
		const auto emplaceReturnIt = m_primeData.emplace(_Where, std::forward<DataType>(_Val));
		emplace_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(emplaceDistance, std::forward<DataType>(_Val));
		return emplaceReturnIt;
	}

	const typename std::vector<DataType>::const_iterator erase(typename std::vector<DataType>::const_iterator _Where)
	{
		const unsigned int eraseDistance = std::distance(m_primeData.cbegin(), _Where);
		const auto eraseReturnIt = m_primeData.erase(_Where);
		erase_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(eraseDistance);
		return eraseReturnIt;
	}

	const typename std::vector<DataType>::const_iterator erase(typename std::vector<DataType>::const_iterator _First, typename std::vector<DataType>::const_iterator _Last)
	{
		const unsigned int firstEraseDistance = std::distance(m_primeData.cbegin(), _First);
		const unsigned int lastEraseDistance = std::distance(m_primeData.cbegin(), _Last);
		const auto eraseReturnIt = m_primeData.erase(m_primeData.cbegin() + firstEraseDistance, m_primeData.cbegin() + lastEraseDistance);
		erase_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(firstEraseDistance, lastEraseDistance);
		return eraseReturnIt;
	}

	void push_back(DataType&& _Val)
	{
		m_primeData.push_back(std::forward<DataType>(_Val));
		push_back_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(std::forward<DataType>(_Val));
	}

	void emplace_back(DataType&& _Val)
	{
		m_primeData.emplace_back(std::forward<DataType>(_Val));
		emplace_back_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(std::forward<DataType>(_Val));
	}

	void pop_back()
	{
		m_primeData.pop_back();
		pop_back_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>();
	}

	void resize(size_t _NewSize)
	{
		m_primeData.resize(_NewSize);
		resize_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(_NewSize);
	}

	void resize(size_t _NewSize, DataType&& _Val)
	{
		m_primeData.resize(_NewSize, _Val);
		resize_memoized<static_cast<int>(std::tuple_size<decltype(m_memoizedData)>::value) - 1>(_NewSize, std::forward<DataType>(_Val));
	}

private:
	/** Clear() */
	template<int index>
	void clear_memoized(typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void clear_memoized(typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.clear();
		clear_memoized<index - 1>();
	}

	/* Insert */
	template<int index>
	void insert_memoized(unsigned int iterOffset, DataType&& _Val, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void insert_memoized(unsigned int iterOffset, DataType&& _Val, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.insert(std::get<index>(m_memoizedData).second.cbegin() + iterOffset, std::get<index>(m_memoizedData).first(std::forward<DataType>(_Val)));
		insert_memoized<index - 1>(iterOffset, std::forward<DataType>(_Val));
	}

	/* Emplace */
	template<int index>
	void emplace_memoized(unsigned int iterOffset, DataType&& _Val, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void emplace_memoized(unsigned int iterOffset, DataType&& _Val, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.emplace(std::get<index>(m_memoizedData).second.cbegin() + iterOffset, std::get<index>(m_memoizedData).first(std::forward<DataType>(_Val)));
		emplace_memoized<index - 1>(iterOffset, std::forward<DataType>(_Val));
	}

	/* Erase */
	template<int index>
	void erase_memoized(unsigned int iterOffset, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void erase_memoized(unsigned int iterOffset, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.erase(std::get<index>(m_memoizedData).second.cbegin() + iterOffset);
		erase_memoized<index - 1>(iterOffset);
	}

	/* Erase */
	template<int index>
	void erase_memoized(unsigned int firstIterOffset, unsigned int lastIterOffset, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void erase_memoized(unsigned int firstIterOffset, unsigned int lastIterOffset, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.erase(std::get<index>(m_memoizedData).second.cbegin() + firstIterOffset, std::get<index>(m_memoizedData).second.cbegin() + lastIterOffset);
		erase_memoized<index - 1>(firstIterOffset, lastIterOffset);
	}

	/* Push_Back */
	template<int index>
	void push_back_memoized(DataType&& _Val, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void push_back_memoized(DataType&& _Val, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.push_back(std::get<index>(m_memoizedData).first(std::forward<DataType>(_Val)));
		push_back_memoized<index - 1>(std::forward<DataType>(_Val));
	}

	/* Emplace_Back */
	template<int index>
	void emplace_back_memoized(DataType&& _Val, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void emplace_back_memoized(DataType&& _Val, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.emplace_back(std::get<index>(m_memoizedData).first(std::forward<DataType>(_Val)));
		emplace_back_memoized<index - 1>(std::forward<DataType>(_Val));
	}

	/* Pop_Back */
	template<int index>
	void pop_back_memoized(typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void pop_back_memoized(typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.pop_back();
		pop_back_memoized<index - 1>();
	}

	/* Resize */
	template<int index>
	void resize_memoized(size_t _NewSize, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void resize_memoized(size_t _NewSize, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.resize(_NewSize);
		resize_memoized<index - 1>(_NewSize);
	}

	/* Resize */
	template<int index>
	void resize_memoized(size_t _NewSize, DataType&& _Val, typename std::enable_if<(index < 0), int>::type = 0) { return; }
	template<int index>
	void resize_memoized(size_t _NewSize, DataType&& _Val, typename std::enable_if<(index >= 0), int>::type = 0)
	{
		std::get<index>(m_memoizedData).second.resize(_NewSize, std::get<index>(m_memoizedData).first(std::forward<DataType>(_Val)));
		resize_memoized<index - 1>(_NewSize, std::forward<DataType>(_Val));
	}

	/*This is the main data storage, basic vector of the type you have provided. You can access it via getPrimeData();*/
    std::vector<DataType> m_primeData;

	/* This is the meat of the class, the memoized vector containers
	 * Its a tuple of the same size of the number of transformations you provide when initialising this vector
	 * Each tuple element is a pair of the transformation, and a vector which contains resultant-type object of applying that transformation to the primeData type
	 * You can access this externally with getMemoizedData<memoizationIndex>() */
    std::tuple<std::pair<Transformation&&, std::vector<typename std::result_of<Transformation(DataType)>::type>>...> m_memoizedData;
};