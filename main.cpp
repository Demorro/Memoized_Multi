#include <iostream>
#include <algorithm>
#include <string>
#include "MemoizedMultiVec.h"
#include "MemoizedMultiVec_Tests.h"
#include <assert.h>

int main()
{
	assert(MemoizedMultiVec_Tests::runTests());
    auto memoize1 = [](double i) {return i * 10; };
    auto memoize2 = [](double i) {return std::to_string(i); };

    //Issues. Dont want to have to do the decltypes or the moves, something must be up.
    MemoizedMultiVec<double, decltype(memoize1), decltype(memoize2)> memoizedVec(std::move(memoize1), std::move(memoize2));

    memoizedVec.push_back(1);
    memoizedVec.push_back(5);
    memoizedVec.push_back(15);

    //Should now have 3 vectors, of {1.0,5.0,15.0} and {10.0.,50.0,150.0} and {"1", "5" and "15"}


	std::cout << "Type of return : " << typeid(memoizedVec.getMemoizedData<0>()).name() << std::endl;

    std::cout << "Priting prime data : " << std::endl;
    for (const auto& val : memoizedVec.getPrimeData())
    {
        std::cout << "TYPE: " << typeid(val).name() << ", Value: " << val << std::endl;
    }
	std::cout << std::endl;
    
    std::cout << "Priting memoized data 1: " << std::endl;
    for (const auto& val : memoizedVec.getMemoizedData<0>())
    {
		std::cout << "TYPE: " << typeid(val).name() << ", Value: " << val << std::endl;
    }
	std::cout << std::endl;

	std::cout << "Priting memoized data 2: " << std::endl;
	for (const auto& val : memoizedVec.getMemoizedData<1>())
	{
		std::cout << "TYPE: " << typeid(val).name() << ", Value: " << val << std::endl;
	}
	std::cout << std::endl;

    std::cout << std::cin.get();
    return 0;
}