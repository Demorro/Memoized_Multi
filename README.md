**MemoizedMultiVec**

This container is designed to be a nice interface to create a set of vectors that track a "prime" vector container according to a set of transformations.
This might be useful if you, for example, have a set of matrix transforms, and you want to store vertices in a container, but also keep a memoized version
of all the transformed vertices available for easy access relative to the prime container. Transformations are done at insert time, so access is quick after.
There is no need for the memoized vectors to be of similar types, and indeed this might be a handy way to store the same data as various types without having to do
on-the-fly conversion. Types of the memoized vectors will be of the resultant type of the provided transformations.


Example usage :  
```c++
auto times10 = [](double i) {return i * 10; };  
auto toString = [](double i) {return std::to_string(i); };  
MemoizedMultiVec<double, decltype(times10), decltype(toString)> memoizedVec(std::move(times10), std::move(toString)); 
```
 
 
This creates 3 vectors, the prime vector of type double, then another vector of type double, and a vector of type string.
Any insertion to the vector will now also insert a double in the second vector, which is 10 times bigger than the first, and a string representation of the data in the third vector

```c++
memoizedVec.push_back(5);  
memoizedVec.getPrimeData()[0] == 5.0;  
memoizedVec.getMemoizedData<0>()[0] == 50.0;  
memoizedVec.getMemoizedData<1>()[0] == "5.0"  
```

Transformations can be lambdas, std::functions, or really any callable object. Lambdas are probably best.
 
 
Notes : A BIG limitation of this class is that you can't really use any modifying standard algorithms with it. I tried to do something with custom iterators, but it defeated me
Currently the only things you can use are the wrapped functions, if you want to say, stable partition this, you're shit out of luck.
Still quite a useful utility tho, in my opinion.  
Would be nice if there were map/list/etc versions, could maybe make that generic and make this MemoizedMultiContainer<Container,Type>, but that might be going a bit too far.
