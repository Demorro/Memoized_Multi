#pragma once

#include "MemoizedMultiVec.h"
#include <string>
#include <sstream>
#include <iomanip>

namespace MemoizedMultiVec_Tests
{
	decltype(auto) buildTestMemoizedVector()
	{
		auto memoize1 = [](double i) {return i * 10; };
		auto memoize2 = [](double i) {std::stringstream stream; stream << std::fixed << std::setprecision(2) << i; return stream.str(); };

		return MemoizedMultiVec<double, decltype(memoize1), decltype(memoize2)>(std::move(memoize1), std::move(memoize2));
	}

	decltype(auto) buildPopulatedTestMemoizedVector()
	{
		auto memoizedVec = buildTestMemoizedVector(); 
		memoizedVec.push_back(5.0);
		memoizedVec.push_back(15.0);
		memoizedVec.push_back(-3.5);
		memoizedVec.push_back(0.0);

		return memoizedVec;
	}

	bool testPushBack()
	{
		auto memoizedVec = buildTestMemoizedVector();
		memoizedVec.push_back(5.0);
		memoizedVec.push_back(10.0);
		memoizedVec.push_back(-3.5);
		memoizedVec.push_back(0.0);

		if (memoizedVec.getPrimeData() != std::vector<double>{5.0, 10.0, -3.5, 0.0}) { return false; }
		if (memoizedVec.getMemoizedData<0>() != std::vector<double>{50.0, 100.0, -35.0, 0.0}) { return false; }
		if (memoizedVec.getMemoizedData<1>() != std::vector<std::string>{"5.00", "10.00", "-3.50", "0.00"}) { return false; }
		return true;
	}

	bool testEmplaceBack()
	{
		auto memoizedVec = buildTestMemoizedVector();
		memoizedVec.emplace_back(5.0);
		memoizedVec.emplace_back(10.0);
		memoizedVec.emplace_back(-3.5);
		memoizedVec.emplace_back(0.0);

		if (memoizedVec.getPrimeData() != std::vector<double>{5.0, 10.0, -3.5, 0.0}) { return false; }
		if (memoizedVec.getMemoizedData<0>() != std::vector<double>{50.0, 100.0, -35.0, 0.0}) { return false; }
		if (memoizedVec.getMemoizedData<1>() != std::vector<std::string>{"5.00", "10.00", "-3.50", "0.00"}) { return false; }
		return true;
	}

	bool testClear()
	{
		auto memoizedVec = buildPopulatedTestMemoizedVector();
		memoizedVec.clear();

		if (memoizedVec.getPrimeData().size() != 0) { return false; }
		if (memoizedVec.getMemoizedData<0>().size() != 0) { return false; }
		if (memoizedVec.getMemoizedData<1>().size() != 0) { return false; }

		memoizedVec.push_back(5.0);
	    if (memoizedVec.getPrimeData().size() != 1) { return false; }
		if (memoizedVec.getMemoizedData<0>().size() != 1) { return false; }
		if (memoizedVec.getMemoizedData<1>().size() != 1) { return false; }

		return true;
	}

	bool testInsert()
	{
		auto memoizedVec = buildPopulatedTestMemoizedVector();
		memoizedVec.insert(memoizedVec.begin() + 2, 100.0);
		if (memoizedVec.getPrimeData()[2] != 100.0) {return false;}
		if (memoizedVec.getMemoizedData<0>()[2] != 1000.0) { return false; }
		if (memoizedVec.getMemoizedData<1>()[2] != "100.00") { return false; }
		return true;
	}

	bool testEmplace()
	{
		auto memoizedVec = buildPopulatedTestMemoizedVector();
		memoizedVec.emplace(memoizedVec.begin() + 2, 100.0);
		if (memoizedVec.getPrimeData()[2] != 100.0) { return false; }
		if (memoizedVec.getMemoizedData<0>()[2] != 1000.0) { return false; }
		if (memoizedVec.getMemoizedData<1>()[2] != "100.00") { return false; }
		return true;
	}

	bool testErase()
	{
		auto memoizedVec = buildPopulatedTestMemoizedVector();
		memoizedVec.erase(memoizedVec.begin() + 1);
		if (memoizedVec.getPrimeData()[1] == 10.0) { return false; }
		if (memoizedVec.getMemoizedData<0>()[1] == 100.0) { return false; }
		if (memoizedVec.getMemoizedData<1>()[1] == "10.00") { return false; }

		if (memoizedVec.getPrimeData() != std::vector<double>{5.0, -3.5, 0.0}) { return false; }
		if (memoizedVec.getMemoizedData<0>()!= std::vector<double>{50.0, -35.0, 0.0}) { return false; }
		
		auto memoizedVec2 = buildPopulatedTestMemoizedVector();
		memoizedVec2.push_back(5.0);
		memoizedVec2.erase(memoizedVec2.begin(), memoizedVec2.end());
		if (memoizedVec2.getPrimeData().size() > 0) { return false; }
		if (memoizedVec2.getMemoizedData<0>().size() > 0) { return false; }
		if (memoizedVec2.getMemoizedData<1>().size() > 0) { return false; }

		return true;
	}

	bool testPopBack()
	{
		auto memoizedVec = buildPopulatedTestMemoizedVector();
		memoizedVec.pop_back();
		if (memoizedVec.getPrimeData().size() != 3) { return false; };
		if (memoizedVec.getMemoizedData<0>().size() != 3) { return false; };
		if (memoizedVec.getMemoizedData<0>().size() != 3) { return false; };
		if (memoizedVec.getPrimeData()[2] != -3.5) { return false; };
		if (memoizedVec.getMemoizedData<0>()[2] != -35.0) { return false; };
		if (memoizedVec.getMemoizedData<1>()[2] != "-3.50") { return false; };
		return true;
	}

	bool testResize()
	{
		auto memoize1 = [](double i) {return i * 10; };
		auto memoize2 = [](double i) {std::stringstream stream; stream << std::fixed << std::setprecision(2) << i; return stream.str(); };

		MemoizedMultiVec<double, decltype(memoize1), decltype(memoize2)> vec(std::move(memoize1), std::move(memoize2));
		vec.resize(5);
		if (vec.getPrimeData().size() != 5) { return false; };
		if (vec.getMemoizedData<0>().size() != 5) { return false; };
		if (vec.getMemoizedData<1>().size() != 5) { return false; };
		if (vec.getPrimeData()[4] != 0.0) { return false; };
		if (vec.getMemoizedData<0>()[4] != 0.0) { return false; };
		if (vec.getMemoizedData<1>()[4] != "") { return false; };

		vec.resize(6, 15.0);
		if (vec.getPrimeData().size() != 6) { return false; };
		if (vec.getMemoizedData<0>().size() != 6) { return false; };
		if (vec.getMemoizedData<1>().size() != 6) { return false; };
		if (vec.getPrimeData()[5] != 15.0) { return false; };
		if (vec.getMemoizedData<0>()[5] != 150.0) { return false; };
		if (vec.getMemoizedData<1>()[5] != "15.00") { return false; };
		return true;
	}

	bool runTests() 
	{
		return testPushBack() &&
			testEmplaceBack() &&
			testClear() &&
			testInsert() &&
			testEmplace() &&
			testErase() &&
			testPopBack() &&
			testResize();
	}
}
